OpenFullFaceShield
=======

Assembly Instructions
-----------

If you have received TheChandlerShield envelope, follow these instructions to assemble your shield:

***Step 1:*** Remove the headband from the envelope.
 
***Step 2:***  Remove the clear shield from the envelope.
Line up the holes on shield with the headband tabs.
Ensure the shield fits securely on the headband.

***Step 3:*** Once the shield is secure, place the headband on the forehead for a snug fit.
Adjust the elastic as needed.

Please refer image (ChandlerShieldAssemblyInstructions.png) for details.

Objective
-----------

This is a very simple full face shield, and designed with the following goals:
- Be better than nothing.
- Fast to manufacture(laser cut in seconds), 
- Use only abundant and easily acquired materials.
- Fast and easy to assemble
- Comfortable for wide range of head sizes.
- Disposable

Materials
-----------
- 1/4(5mm-6mm) Closed cell bpa free EVA Foam sheet (12x18), 
- standard 3 hole punched clear bindersheets, Transparency film, or PETG cut to 11x8.5(if available)
- Shock Cord /Elastic bungie (~3mm diameter, cut to 18in length.. or similar)

Tools
-----------
- laser cutter or cricut Maker
- hole punch
- standard scissors


Disclamer.
------------
This is not a medical device and only to be used for fun, education, or 
party attire during the apocolypse for lack of any other better option.

You must review and agree to the [License Agreement](https://gitlab.com/lucidminds/TheChandlerShield/-/blob/master/LICENSE) before use.
